#!/bin/bash

if sudo docker ps -a | grep nginx_proxy > /dev/null
    then
        echo Removing existing container.......
        sudo docker container rm -f nginx_proxy
    fi
sudo docker run -d --name nginx_proxy -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf --network host nginx:latest