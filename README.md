# Tech_Task
### Example of PROD_ENV or STAGE_ENV
```bash
WORDPRESS_PORT=8080
MYSQL_PORT=3306
WORDPRESS_DB_USER=username
WORDPRESS_DB_PASSWORD=password
WORDPRESS_DB_NAME=dbname
MYSQL_ROOT_PASSWORD=rootpassword
```

### Example of ZABBIX_ENV
```bash
POSTGRES_USER=username
POSTGRES_PASSWORD=password
POSTGRES_DB=dbname
```

